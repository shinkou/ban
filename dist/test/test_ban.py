# vim: ff=unix fileencoding=utf-8 lcs=tab\:>. list noet sc sw=4 ts=4 tw=0
from ban import utils
import unittest

class BanTestCase(unittest.TestCase):
	def test_sanitize_banned(self):
		obj = {'words':'RAPE','services':'all'}
		banned = utils.sanitize_banned(obj)
		self.assertEqual(banned.get('words'), 'rape')
		self.assertEqual(banned.get('services'), 'ALL')

if '__main__' == __name__:
	unittest.main()
