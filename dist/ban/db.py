# vim: ff=unix fileencoding=utf-8 lcs=tab\:>. list noet sc sw=4 ts=4 tw=0
import os, pg8000, sqlalchemy

db = None

async def list_all_banned():
	out = dict()
	with _get_connection() as cnx:
		try:
			rs = cnx.execute('SELECT words, services FROM banned').fetchall()
			out = {
				'status': 'ok'
				, 'results': [{'words': r[0], 'services': r[1]} for r in rs]
			}
		except (Exception, pg8000.Error) as err:
			out = {'status': 'error', 'message': 'Error listing banned words'}
	return out

async def add_banned(obj):
	out = dict()
	if 'words' in obj and 'services' in obj:
		stmt = sqlalchemy.text(
			'INSERT INTO banned (words, services) VALUES (:words, :services) RETURNING words, services'
		)
		with _get_connection() as cnx:
			try:
				res = cnx.execute(
					stmt
					, words=obj.get('words')
					, services=obj.get('services')
				).fetchall()
				res = [{'words': res[0][0], 'services': res[0][1]}]
				out = {'status': 'ok', 'results': res}
			except (Exception, pg8000.Error) as err:
				out = {'status': 'error', 'message': 'Error adding banned word'}
				cnx.rollback()
	return out

async def delete_banned(obj):
	out = dict()
	if 'words' in obj:
		stmt = sqlalchemy.text(
			'DELETE FROM banned WHERE words = :words RETURNING words, services'
		)
		with _get_connection() as cnx:
			try:
				res = cnx.execute(stmt, words=obj.get('words')).fetchall()
				res = [{'words': res[0][0], 'services': res[0][1]}]
				out = {'status': 'ok', 'results': res}
			except (Exception, pg8000.Error) as err:
				out = {'status': 'error', 'message': 'Error deleting banned word'}
				cnx.rollback()
	return out

async def update_banned(obj):
	out = dict()
	if 'words' in obj and 'services' in obj:
		stmt = sqlalchemy.text(
			'UPDATE banned SET services = :services WHERE words = :words RETURNING words, services'
		)
		with _get_connection() as cnx:
			try:
				res = cnx.execute(
					stmt
					, words=obj.get('words')
					, services=obj.get('services')
				).fetchall()
				res = [{'words': res[0][0], 'services': res[0][1]}]
				out = {'status': 'ok', 'results': res}
			except (Exception, pg8000.Error) as err:
				out = {'status': 'error', 'message': 'Error updating banned word'}
				cnx.rollback()
	return out

def _init_connection_pool(db_config):
	db_user = os.environ['DB_USER']
	db_pass = os.environ['DB_PASS']
	db_host = os.environ['DB_HOST']
	db_port = os.environ['DB_PORT']
	db_name = os.environ['DB_NAME']
	pool = sqlalchemy.create_engine(
		sqlalchemy.engine.url.URL(
			drivername='postgresql+pg8000'
			, username=db_user
			, password=db_pass
			, host=db_host
			, port=db_port
			, database=db_name
		)
		, **db_config
	)
	pool.dialect.description_encoding = None
	return pool

def _get_connection():
	global db
	if db is None:
		db_config = {
			"pool_size": 5
			, "max_overflow": 2
			, "pool_timeout": 30
			, "pool_recycle": 1800
		}
		db = _init_connection_pool(db_config)
	return db.connect()
