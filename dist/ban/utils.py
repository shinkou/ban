# vim: ff=unix fileencoding=utf-8 lcs=tab\:>. list noet sc sw=4 ts=4 tw=0

def sanitize_banned(some_obj):
	if isinstance(some_obj, dict):
		cloned_dict = some_obj.copy()
		if 'words' in some_obj:
			cloned_dict['words'] = sanitize_words(cloned_dict.get('words'))
		if 'services' in some_obj:
			cloned_dict['services'] = sanitize_services(cloned_dict.get('services'))
		return cloned_dict

def sanitize_words(some_str):
	if isinstance(some_str, str):
		return some_str.lower()

def sanitize_services(some_str):
	if isinstance(some_str, str):
		return some_str.upper()
