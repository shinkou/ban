# vim: ff=unix fileencoding=utf-8 lcs=tab\:>. list noet sc sw=4 ts=4 tw=0
from . import db, utils
from quart import Quart, request

def create_app(test_config=None):
	app = Quart(__name__)

	if test_config is None:
		app.config.from_pyfile('config.py', silent=True)
	else:
		app.config.from_mapping(test_config)

	@app.route('/', methods=['GET'])
	async def list_all_banned():
		return await db.list_all_banned()

	@app.route('/', methods=['POST'])
	async def add_banned_word():
		return await db.add_banned(
			utils.sanitize_banned(await request.get_json())
		)

	@app.route('/', methods=['DELETE'])
	async def delete_banned_word():
		return await db.delete_banned(
			utils.sanitize_banned(await request.get_json())
		)

	@app.route('/', methods=['PUT'])
	async def update_banned_word():
		return await db.update_banned(
			utils.sanitize_banned(await request.get_json())
		)

	return app
