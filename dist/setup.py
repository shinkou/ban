#!/usr/bin/env python3
# vim: ff=unix fileencoding=utf-8 lcs=tab\:>. list noet sc sw=4 ts=4 tw=0
from setuptools import setup

setup(
	name = 'ban'
	, version = '0.1'
	, packages = ['ban']
	, install_requires = ['quart', 'pg8000', 'sqlalchemy']
	, author = 'Dennis Wong'
	, author_email = 'shinkou@outlook.com'
	, description = 'Ban'
	, url = 'https://gitlab.com/shinkou/ban'
)
