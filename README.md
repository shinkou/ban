# ban

A simple banned word management system written in Python3 using quart with
PostgreSQL as backend database.

## Requirements

- docker
- docker-compose

## How to Run

1. Instantiate a local cluster with the following command

```
$ docker-compose up -d
```

2. Insert entries with the `POST` API like this

```
$ curl -X POST -H 'Content-Type: application/json' \
 --data-raw '{"words":"piss","services":"all"}' \
 'http://localhost:5000/'
```

3. Review the added entries with the `GET` API like this:

```
$ curl -X GET 'http://localhost:5000/'
```

4. Remove entries with the `DELETE` API like this

```
$ curl -X DELETE -H 'Content-Type: application/json' \
 --data-raw '{"words":"piss"}' \
 'http://localhost:5000/'
```

5. Updates are done with the `PUT` API in the following fashion

```
$ curl -X PUT -H 'Content-Type: application/json' \
 --data-raw '{"words":"piss","services":"related_search"}' \
 'http://localhost:5000/'
```

## How to Stop

1. Bring down the entire local cluster with the following command

```
$ docker-compose down
```

2. Remove the compiled docker image if necessary

```
$ docker rmi ban\_http
```

That's it. Enjoy!
